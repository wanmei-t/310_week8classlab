package geometry;

public class LotsOfShapes {
    public static void main(String[] args)
    {
        Shape[] shapes = new Shape[5];
        shapes[0] = new Rectangle(2, 5);
        shapes[1] = new Rectangle(6, 3);
        shapes[2] = new Circle(16);
        shapes[3] = new Circle(20);
        shapes[4] = new Square(16);

        for (int i = 0; i < shapes.length; i++)
        {
            System.out.println(shapes[i].getArea() + " unit squared and " + shapes[i].getPerimeter() + " units.");
        }
    }
}
